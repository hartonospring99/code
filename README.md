###############  Mini Project ini berisi implementasi CRUD Blogs #############

gitlab      : https://gitlab.com/hartonospring99/code

docker      : https://hub.docker.com/r/hartonospringboot/code-blogs

              docker pull hartonospringboot/code-blogs
              
              Jalankan Aplikasi : docker run -p 8080:8080 hartonospringboot/code-blogs:v1

postman     : CHALLANGE.postman_collection (\code\CHALLANGE.postman_collection)

X-Key       : b591c345-f559-4188-8808-765a35508f08

1. Path get list all    : http://localhost:8080/api/blogposts/list

2. Path get by id       : http://localhost:8080/api/blogposts/find/{id}

3. Path update          : http://localhost:8080/api/blogposts/update

                            body : {
                                        "id": 10,
                                        "title": "Testing Update",
                                        "content": "Update Content",
                                        "author": "Autor Update"
                                    }

4. Path insert data     : http://localhost:8080/api/blogposts/posting

                            body : {
                                        "title": "Testing Update",
                                        "content": "Update Content",
                                        "author": "Autor Update"
                                    }

5. Path delete by id    : http://localhost:8080/api/blogposts/delete/{id}

Constraints:
- Use H2 as temporary DB
- All endpoints return appropriate HTTP status codes.
- All endpoints follow RESTful API conventions.
- Use Spring Boot with Spring Data JPA for data persistence.
- Implement pagination for the list of blog posts endpoint.
- Implement sorting for the list of blog posts endpoint.
- Add authentication and authorization to the API [Header X-Key]
- Use Docker to containerize the application.
