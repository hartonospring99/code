package com.example.code.controller;

import com.example.code.model.BlogPost;
import com.example.code.services.FindByIdServices;
import com.example.code.util.BaseHttpResponse;
import com.example.code.util.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/blogposts")
public class FindByIdController {
    private BaseHttpResponse response = new BaseHttpResponse();
    private BaseResponse resp = new BaseResponse();
    @Autowired
    FindByIdServices findByIdServices;

    @GetMapping("/find/{id}")
    public ResponseEntity<BlogPost> getBlogPostById(@PathVariable Long id) {
        resp = findByIdServices.getData(id);
        return response.baseHttpResponse(resp);
    }
}
