package com.example.code.controller;

import com.example.code.services.ListAllServices;
import com.example.code.util.BaseHttpResponse;
import com.example.code.util.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/api/blogposts")
public class ListAllController {
    private BaseHttpResponse response = new BaseHttpResponse();
    private BaseResponse resp = new BaseResponse();
    @Autowired
    ListAllServices listAllServices;

    @PostMapping("/list")
    public ResponseEntity getList(@RequestBody Map<String, Integer> params) {
        resp = listAllServices.getList(params);
        return response.baseHttpResponse(resp);
    }
}
