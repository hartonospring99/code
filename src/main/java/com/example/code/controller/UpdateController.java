package com.example.code.controller;

import com.example.code.model.BlogPost;
import com.example.code.services.UpdateServices;
import com.example.code.util.BaseHttpResponse;
import com.example.code.util.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/api/blogposts")
public class UpdateController {
    private BaseHttpResponse response = new BaseHttpResponse();
    private BaseResponse resp = new BaseResponse();
    @Autowired
    UpdateServices updateServices;

    @PostMapping("/update")
    public ResponseEntity<BlogPost> update(@RequestBody Map<String, Object> params) {
        resp = updateServices.update(params);
        return response.baseHttpResponse(resp);
    }
}
