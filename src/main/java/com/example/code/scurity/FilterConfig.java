package com.example.code.scurity;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FilterConfig {
    @Bean
    public FilterRegistrationBean<XKeyHeaderFilter> xKeyHeaderFilter() {
        FilterRegistrationBean<XKeyHeaderFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new XKeyHeaderFilter());
        registrationBean.addUrlPatterns("/*");
        return registrationBean;
    }
}




