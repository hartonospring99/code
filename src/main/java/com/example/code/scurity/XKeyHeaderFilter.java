package com.example.code.scurity;

import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class XKeyHeaderFilter implements Filter {

    private static final String EXPECTED_X_KEY = "b591c345-f559-4188-8808-765a35508f08";

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        String receivedXKey = httpRequest.getHeader("X-Key");

        if (httpRequest.getRequestURI().startsWith("/api")) {
            if (EXPECTED_X_KEY.equals(receivedXKey)) {
                chain.doFilter(request, response);
            } else {
                HttpServletResponse httpResponse = (HttpServletResponse) response;
                httpResponse.setStatus(HttpServletResponse.SC_FORBIDDEN);
                httpResponse.getWriter().write("Unauthorized Access");
            }
        } else {
            chain.doFilter(request, response);
        }
    }
}
