package com.example.code.services;

import com.example.code.model.BlogPost;
import com.example.code.repository.BlogPostRepository;
import com.example.code.util.BaseResponse;
import com.example.code.util.ConstantUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;
import java.util.Optional;

@Service
public class FindByIdServices {

    @Autowired
    BlogPostRepository blogPostRepository;

    public BaseResponse getData(Long id) {
        BaseResponse resp = new BaseResponse();
        Optional<BlogPost> blogPost = blogPostRepository.findById(id);
        try {
            if (!blogPost.isEmpty()) {
                resp.result = ConstantUtil.SUCCESS_STATUS;
                resp.message = ConstantUtil.SUCCESS;
                resp.payload = blogPost;
                return resp;
            } else {
                resp.result = ConstantUtil.STATUS_404;
                resp.message = ConstantUtil.NOT_FOUND;
                return resp;
            }
        } catch (NoResultException e) {
            resp.result = ConstantUtil.FAILED_STATUS;
            resp.message = ConstantUtil.FAILED_ERROR;
            return resp;
        }
    }
}
