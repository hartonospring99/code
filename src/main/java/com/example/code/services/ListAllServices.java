package com.example.code.services;

import com.example.code.model.BlogPost;
import com.example.code.repository.BlogPostRepository;
import com.example.code.util.BaseResponse;
import com.example.code.util.ConstantUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;
import java.util.List;
import java.util.Map;

@Service
public class ListAllServices {

    @Autowired
    BlogPostRepository blogPostRepository;

    public BaseResponse getList(Map<String, Integer> params) {
        BaseResponse resp = new BaseResponse();
        Integer limit = params.get("limit");
        Integer offset = params.get("offset");
        try {
            Pageable pageable = PageRequest.of(offset, limit, Sort.by("id").ascending());
            Page<BlogPost> blogPostPage = blogPostRepository.findAll(pageable);
            List<BlogPost> blogPosts = blogPostPage.getContent();
            if (blogPosts.size() > 0) {
                resp.result = ConstantUtil.SUCCESS_STATUS;
                resp.message = ConstantUtil.SUCCESS;
                resp.payload = blogPosts;
                resp.totalRecords = Math.toIntExact(blogPostPage.getTotalElements());
                resp.totalRecordsFiltered = blogPostPage.getSize();
                return resp;
            } else {
                resp.result = ConstantUtil.STATUS_404;
                resp.message = ConstantUtil.NOT_FOUND;
                return resp;
            }
        } catch (NoResultException e) {
            resp.result = ConstantUtil.FAILED_STATUS;
            resp.message = ConstantUtil.FAILED_ERROR;
            return resp;
        }
    }
}
