package com.example.code.services;

import com.example.code.model.BlogPost;
import com.example.code.repository.BlogPostRepository;
import com.example.code.util.BaseResponse;
import com.example.code.util.ConstantUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

@Service
public class UpdateServices {
    @Autowired
    BlogPostRepository blogPostRepository;

    public BaseResponse update(Map<String, Object> params) {
        BaseResponse resp = new BaseResponse();
        Integer id = (Integer) params.get("id");
        Optional<BlogPost> existingBlogPost = blogPostRepository.findById(id.longValue());
        if (existingBlogPost.isPresent()) {
            BlogPost blogPost = existingBlogPost.get();
            if (params.containsKey("title")) {
                blogPost.setTitle(String.valueOf(params.get("title")));
            }
            if (params.containsKey("content")) {
                blogPost.setContent(String.valueOf(params.get("content")));
            }
            if (params.containsKey("author")) {
                blogPost.setAuthor(String.valueOf(params.get("author")));
            }
            blogPostRepository.save(blogPost);
            resp.result = ConstantUtil.SUCCESS_STATUS;
            resp.message = ConstantUtil.SUCCESS;
            resp.payload = existingBlogPost;
            return resp;
        } else {
            resp.result = ConstantUtil.STATUS_404;
            resp.message = ConstantUtil.NOT_FOUND;
            return resp;
        }
    }
}
