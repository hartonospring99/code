package com.example.code.util;

import org.springframework.http.ResponseEntity;

public class BaseHttpResponse {
    public ResponseEntity baseHttpResponse(BaseResponse resp) {
        if (resp.result == 200) {
            return ResponseEntity.ok(resp);
        } else if (resp.result == 404) {
            return ResponseEntity.status(404).body(resp);
        } else if (resp.result == 400) {
            return ResponseEntity.status(400).body(resp);
        } else if (resp.result == 500) {
            return ResponseEntity.status(500).body(resp);
        } else {
            return ResponseEntity.status(500).body(resp);
        }
    }
}

