package com.example.code.util;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BaseResponse<T> implements Serializable {

    public Integer result;

    public String message;

    public T payload;

    public Integer totalRecords;

    public Integer totalRecordsFiltered;

    @Override
    public String toString() {
        return "BaseResponse{" +
                "result=" + result +
                ", message='" + message + '\'' +
                ", payload=" + payload +
                ", totalRecords=" + totalRecords +
                ", totalRecordsFiltered=" + totalRecordsFiltered +
                '}';
    }
}
