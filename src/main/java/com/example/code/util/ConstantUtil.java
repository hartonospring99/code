package com.example.code.util;

public class ConstantUtil {
    public static final Integer SUCCESS_STATUS = 200;
    public static final String SUCCESS = "success";
    public static final Integer FAILED_STATUS = 500;
    public static final String FAILED_ERROR = "Data tidak diproses";
    public static final Integer STATUS_400 = 400;
    public static final String BAD_REQUES = "Payload Tidak Sesuai";
    public static final Integer STATUS_404 = 404;
    public static final String NOT_FOUND = "Data tidak ditemukan";
}
