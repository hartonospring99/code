package com.example.code.util;

import com.example.code.model.BlogPost;
import com.example.code.repository.BlogPostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class DataInitializer implements ApplicationRunner {
    @Autowired
    BlogPostRepository blogPostRepository;

    @Override
    public void run(ApplicationArguments args) {
        for (int i = 1; i <= 15; i++) {
            BlogPost initialPost = new BlogPost();
            initialPost.setTitle("Judul Post Ke - " + i);
            initialPost.setContent("Konten Post Ke - " + i);
            initialPost.setAuthor("Penulis Ke - " + i);
            blogPostRepository.save(initialPost);
        }
    }
}
